import requests
import sys

# Uncomment to debug requests
# import logging
# logging.basicConfig(level=logging.DEBUG)

if __name__ == "__main__":
    import weatherservice
else:
    from weatherservices import weatherservice
# http://opendata.smhi.se/apidocs/metfcst/get-forecast.html


class Smhi(weatherservice.WeatherService):
    def get_rain_estimates(self):
        return self.get_data(data_point_name="pmean")

    def get_data(self, data_point_name):
        values = []
        smhi_url_base = "https://opendata-download-metfcst.smhi.se"
        smhi_url = "{base_url}/api/category/pmp3g/version/2/geotype/point/lon/{lon}/lat/{lat}/data.json".format(base_url=smhi_url_base, lon=self.lon, lat=self.lat)
        try:
            req = requests.get(smhi_url, verify=False)
            data = req.json()
        except Exception as e:
            print("Unexpected error:", sys.exc_info()[0])
            print(e)
            return None

        timeseries = data['timeSeries']
        for ts in timeseries[:self.hours]:
            for p in ts['parameters']:
                if p['name'] == data_point_name:
                    values.append(p['values'][0])
        return values


if __name__ == '__main__':
    cities = {
        'stockholm': (59.3333, 18.05),
        'linkoping': (58.41086, 15.62157),
        'lund': (55.7058, 13.1932),
    }
    smhi = Smhi(cities['lund'])
    print(smhi.get_rain_estimates())
